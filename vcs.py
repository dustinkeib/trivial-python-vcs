#!/usr/bin/env python3
"""vcs - Trivial Version Control System.

The MIT License (MIT)
Copyright (c) 2016 Dustin Keib

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use, copy,
modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the
following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

All other works copyright their respective owners.
License information on all included works can be found in ./lib

Usage:  vcs add [options] <file>...
        vcs clean
        vcs commit [options] <file>...
        vcs diff [<file>...]
        vcs dump
        vcs ls [<file>...]
        vcs rm [options] <file>...
        vcs args

Commands:
    add         Add file contents to the index
    clean       Delete current repo
    commit      Record changes to the repository
    dump        Print DB (text)
    diff        Show differences between files and index
    ls          List files in index
    rm          Remove file from the index
    args        Developer Test Mode

Options:
    -h, --help      Show this message and exit.
    --version       Show the version and exit
    -v, --verbose   Display Verbose Output
    -q, --quiet     No output
    -b <branch>     create and checkout a new branch
    -B <branch>     create/reset and checkout a branch

Not yet implemented:
    init       Initialize new repo
    branch     List, create, or delete branches
    checkout   Checkout a branch or paths to the working tree
               vcs checkout [options] <branch> -- <file>...
    push       Update remote refs along with associated objects
    clone      Clone a repository into a new directory
    remote     Manage set of tracked repositories
    status     Show status of files in the repository

See 'trivial-python-vcs help <command>' for more information on a specific command.
"""

__version__ = 'Tiny Python Version Control System v0.0.1'
__author__ = 'Dustin Keib'
__email__ = 'dustin@keib.net'

from lib.docopt import docopt
from lib.repo import Repo
from lib.utils import colors
import os
from pprint import pprint
import sys
from time import time
# from tinydb import Query
import uuid


def genuuid():
    """Generate UUID."""
    return str(uuid.uuid4()).replace('-', '')


def sizeof_fmt(num, suffix='B'):
    """Get human readable size suffix."""
    for unit in ['', 'K', 'M', 'G', 'T', 'P', 'E', 'Z']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)


def main(args):
    """Main program."""
    # Start timer
    start = time()

    repofile = 'repo.json'
    repo = Repo(repofile)
    commits = ''

    if args['args'] is True:
        print('\n{}Args:\n{}{}\n'
              .format(colors.fg.purple, colors.fg.darkgrey, args))
        print(docopt(__doc__))
        sys.exit()

    # elif args['--verbose'] is True:
    #     repo.config.update({'debug': 'True'}, Query().debug == 'False')
    #     repo.debug = True

    # elif args['--quiet'] is True:
    #     repo.config.update({'debug': 'False'}, Query().debug == 'True')
    #     repo.debug = False

    elif args['clean'] is True:
        if (input("About to remove {}, are you sure?\n[y/N]: "
                  .format(repofile)).lower() in ('y', 'yes')):
            if os.path.isfile(repofile):
                os.remove(repofile)
                print('Deleted {}.'.format(repofile))
            else:
                print('{} Not Found.'.format(repofile))
            sys.exit()
        else:
            print('Aborted.')
            sys.exit()

    elif args['add'] is True:
        repo.addToIndex(repo.genFileList(args['<file>']))

    elif args['rm'] is True:
        repo.rmFromIndex(repo.genFileList(args['<file>']))

    elif args['ls'] is True:
        if not args['<file>']:
            filelist = repo.getCurrentIndex()
        else:
            filelist = repo.genFileList(args['<file>'])
        if filelist:
            repo.indexList(filelist)
        else:
            print('{}No files found in Index.'.format(colors.fg.purple))

    elif args['commit'] is True:
        uid = '{}@email.address'.format(genuuid())
        commitmsg = uid

        if args['<file>']:
            commits = 0
            files = repo.getFilesFromIndex(repo.genFileList(args['<file>']))

            nManifests = len(repo.manifests.all())
            if nManifests > 0:
                diff = repo.getDiff(files)[0]

            else:
                diff = ''

            if nManifests == 0 or diff != '':
                commits = len(files)
                if files:
                    try:
                        parent = repo.getLastManifest()['ID']
                    except TypeError:
                        parent = ''
                    try:
                        baseline = repo.getManifestByEID(1)['ID']
                    except (TypeError, UnboundLocalError):
                        baseline = parent

                    print('\n{}BASELINE: {}{}\n{}PARENT: {}{}'
                          .format(colors.fg.purple,
                                  colors.fg.darkgrey, baseline,
                                  colors.fg.purple,
                                  colors.fg.darkgrey, parent))

                    repo.do_commit(files, baseline, parent, uid, commitmsg)

                else:
                    print('Please add files to index.')
            else:
                print('Nothing to commit.')
        else:
            print('Please specify 1 or more files.')

    elif args['dump'] is True:
        print('{}Artifacts:{}\n'.format(colors.fg.purple, colors.fg.darkgrey))
        pprint(repo.artifacts.all())
        print('\n{}Manifests:{}\n'.format(colors.fg.purple, colors.fg.darkgrey))
        pprint(repo.manifests.all())

    elif args['diff'] is True:
        if not args['<file>']:
            filelist = repo.getCurrentIndex()

        else:
            filelist = repo.getFilesFromIndex(repo.genFileList(args['<file>']))

        if filelist:
            print(repo.getDiff(filelist)[0])

        else:
            print('{}No files found in Index.'.format(colors.fg.purple))

    elif args['branch'] is True:
        pass

    elif args['checkout'] is True:
        pass
        # if args['<file>'] is True:
        #
        #     if args['BRANCH'] is not None:
        #         if args['-b'] is True:
        #             repo.createBranch(args['BRANCH'])
        #             # Create branch
        #             #   Write branch record to db
        #             #   create branch object
        #         else:
        #             repo.switchBranch(args['BRANCH'])
        #     else:
        #         print('Please specify branch.')

    if repo.debug:
        print('\n{}Args:\n{}{}\n'
              .format(colors.fg.purple, colors.fg.darkgrey, args))

        if commits:
            print('{}Commits: {}{}'.format(colors.fg.purple, colors.fg.darkgrey,
                                           commits))

        print('{}Repo Size: {}{}'.format(colors.fg.purple, colors.fg.darkgrey,
                                         sizeof_fmt(os.path.getsize(repofile))))

        endtime = (time() - start)
        if endtime < 1:
            endtime = endtime * 1000
            timescale = 'ms'
        else:
            timescale = 's'
        print('{}Time: {}{}{}'.format(colors.fg.purple, colors.fg.darkgrey,
                                      round(endtime), timescale))

if __name__ == '__main__':
    args = docopt(__doc__, version=__version__)
    main(args)
