"""TinyDB SmartCache.
https://github.com/msiemens/tinydb-smartcache

The MIT License (MIT)

Copyright (c) 2015 Markus Siemens

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from tinydb.database import Table


class SmartCacheTable(Table):
    """
    A Table with a smarter query cache.
    Provides the same methods as :class:`~tinydb.database.Table`.
    The query cache gets updated on insert/update/remove. Useful when in cases
    where many searches are done but data isn't changed often.
    """

    def _write(self, values):
        # Just write data, don't clear the query cache
        self._storage.write(values)

    def insert(self, element):
        # See Table.insert

        # Insert element
        eid = super(SmartCacheTable, self).insert(element)

        # Update query cache
        for query in self._query_cache:
            results = self._query_cache[query]
            if query(element):
                results.append(element)

        return eid

    def insert_multiple(self, elements):
        # See Table.insert_multiple

        # We have to call `SmartCacheTable.insert` here because
        # `Table.insert_multiple` doesn't call `insert()` for every element
        return [self.insert(element) for element in elements]

    def update(self, fields, cond=None, eids=None):
        # See Table.update

        if callable(fields):
            _update = lambda data, eid: fields(data[eid])
        else:
            _update = lambda data, eid: data[eid].update(fields)

        def process(data, eid):
            old_value = data[eid].copy()

            # Update element
            _update(data, eid)
            new_value = data[eid]

            # Update query cache
            for query in self._query_cache:
                results = self._query_cache[query]

                if query(old_value):
                    # Remove old value from cache
                    results.remove(old_value)

                elif query(new_value):
                    # Add new value to cache
                    results.append(new_value)

        self.process_elements(process, cond, eids)

    def remove(self, cond=None, eids=None):
        # See Table.remove

        def process(data, eid):
            # Update query cache
            for query in self._query_cache:

                results = self._query_cache[query]
                try:
                    results.remove(data[eid])
                except ValueError:
                    pass

            # Remove element
            data.pop(eid)

        self.process_elements(process, cond, eids)

    def purge(self):
        # See Table.purge

        super(SmartCacheTable, self).purge()
        self._query_cache.clear()  # Query cache got invalid
