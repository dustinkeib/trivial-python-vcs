#!/usr/bin/env python3
"""repo.py trivial-python-vcs repo class.

The MIT License (MIT)
Copyright (c) 2016 Dustin Keib

All other works copyright their respective owners
"""
from collections import OrderedDict
from datetime import datetime
import hashlib
from lib import config
from lib.deepdiff import DeepDiff
from lib import magic
from lib.smartcache import SmartCacheTable
from lib.utils import colors
import os
from tinydb import Query
from tinydb import TinyDB
from tinydb import where

# Use tinydb smartcache
TinyDB.table_class = SmartCacheTable


class Repo(object):
    """Repo Base Class."""

    def __init__(self, filename):
        """Initialize Repo Object.

        Artifacts:  Blob of file contents indexed by SHA1sum of the Blob.
        Manifests:  Records containing all data on a changeset.
        Index:      Transaction Log of changesets which have been commited.
        """
        self.db = TinyDB(filename, sort_keys=True, indent=2)
        self.filename = filename
        if len(self.db.tables()) > 1:
            print('\n{}trivial-python-vcs{} repo found at {}{}{}.\n'
                  .format(colors.fg.purple, colors.fg.darkgrey, colors.fg.purple,
                          filename, colors.fg.darkgrey))
            self.link_tables()
        else:
            self.link_tables()
            self.init_configs()

    def link_tables(self):
        """Link objects to tables and set cache sizes individually."""
        self.artifacts = self.db.table('Artifacts', cache_size=config.CACHE_ARTIFACTS)
        self.manifests = self.db.table('Manifests', cache_size=config.CACHE_MANIFESTS)
        self.branches = self.db.table('Branches', cache_size=config.CACHE_BRANCHES)
        self.index = self.db.table('Index', cache_size=config.CACHE_INDEX)
        self.config = self.db.table('Configs', cache_size=config.CACHE_CONFIGS)
        self.debug = self.config.search(Query().debug == 'True')

    def init_configs(self):
        """Init Config Table from config.py."""
        self.config.insert({'debug': config.DEBUG,
                            'writeartifacts': config.WRITEARTIFACTS,
                            'cache_artifacts': config.CACHE_ARTIFACTS,
                            'cache_manifests': config.CACHE_MANIFESTS,
                            'cache_branches': config.CACHE_BRANCHES,
                            'cache_index': config.CACHE_INDEX,
                            'cache_configs': config.CACHE_CONFIGS})
        self.branches.insert({'current': config.DEFAULTBRANCH})

    def sort_manifest(self, manifest):
        """Sort Manifest."""
        return OrderedDict(sorted(manifest.items(), key=lambda t: t[0]))

    def create_manifest(self, filelist, baseline, parent, uid, commitmsg):
        """Create a manifest."""
        manifest = {}

        if baseline is not '' and parent is not '':
            manifest['B'] = baseline
            manifest['P'] = parent

        manifest['C'] = commitmsg
        manifest['D'] = datetime.utcnow().isoformat()
        manifest['F'] = self.fcards(filelist)
        manifest['U'] = uid
        manifest = self.sort_manifest(manifest)
        manifest['Z'] = self.returnhash(self.totext(manifest))
        manifest = self.sort_manifest(manifest)

        # FIX: Figure out deduplicated way of doing this
        if baseline is not '' and parent is not '':
            self.manifests.insert({'ID': self.returnhash(self.totext(manifest)),
                                   'B': manifest['B'],
                                   'C': manifest['C'],
                                   'D': manifest['D'],
                                   'F': manifest['F'],
                                   'P': manifest['P'],
                                   'U': manifest['U'],
                                   'Z': manifest['Z']})

        else:
            self.manifests.insert({'ID': self.returnhash(self.totext(manifest)),
                                   'C': manifest['C'],
                                   'D': manifest['D'],
                                   'F': manifest['F'],
                                   'U': manifest['U'],
                                   'Z': manifest['Z']})

        if self.debug:
            print('\n{}Manifest {}:'
                  .format(colors.fg.purple,
                          self.returnhash(self.totext(manifest))))
            print('{}{}'.format(colors.fg.darkgrey, self.totext(manifest)))

    def fcards(self, filelist):
        """Create F records."""
        files = {}
        for filename in filelist:
            if filename:
                if (magic.from_file(filename, mime=True) != (b'application/octet-stream')):
                    files[self.hashfile(filename)] = filename
        if files:
            for sha, fn in files.items():
                blob = self.readfile(fn)
                artifactID = hashlib.sha1(blob.encode('utf-8')).hexdigest()
                dedupID = ''
                try:
                    dedupID = self.artifacts.search(where('ID') == artifactID)[0]['ID']
                except IndexError:
                    pass
                if (dedupID != artifactID):
                    self.artifacts.insert({'ID': artifactID, 'Blob': blob})
        return files

    def getManifestFCards(self, manifestID):
        """Return F Records for given Manifest EID as Dict object."""
        return self.manifests.search(where('F') == manifestID)

    def getBaseLine(self):
        """Get First Manifest."""
        return self.manifests.get(eid=1)

    def getManifestByEID(self, eid):
        """Get Manifest by Record #."""
        return self.manifests.get(eid=eid)

    def getLastManifest(self):
        """Get Last Manifest."""
        return self.manifests.get(eid=len(self.manifests))

    def getManifestEID(self, manifestSHA):
        """Get the Manifest DB EID given a manifest SHA1sum."""
        return self.manifests.get(where('ID') == manifestSHA).eid

    def do_commit(self, filelist, baseline, parent, uid, commitmsg):
        """Create test commit."""
        mbhash = self.create_manifest(filelist, baseline, parent, uid, commitmsg)
        # manifest.test(repo)
        return mbhash

    def getCurrentIndex(self):
            """FIX: parse db to make list of current index, this seems hackish."""
            cur_index = list()
            for frec in self.index.search(where('filename') != ''):
                cur_index.append(frec['filename'])
            return cur_index

    def getFilesFromIndex(self, filelist):
        """Return files from filelist that are in index."""
        results = list()
        cur_index = self.getCurrentIndex()
        for filename in filelist:
            # if filename not in cur_index:
            #     self.printFileEID('NOT FOUND', filename, '')

            # elif filename in cur_index:
            if filename in cur_index:
                # eid = (self.index.get(where('filename') == filename)).eid
                # self.printFileEID('FOUND', filename, eid)
                results.append(filename)
        return results

    def addToIndex(self, filelist):
        """Add files in filelist to index."""
        cur_index = self.getCurrentIndex()
        for filename in filelist:
            if filename not in cur_index:
                eid = self.index.insert({'filename': filename})
                self.printFileEID('ADDED', filename, eid)
            elif filename in cur_index:
                eid = (self.index.get(where('filename') == filename)).eid
                self.printFileEID('FOUND', filename, eid)

    def rmFromIndex(self, filelist):
        """Remove filelist from the index."""
        cur_index = self.getCurrentIndex()
        for filename in filelist:
            if filename not in cur_index:
                self.printFileEID('NOT FOUND', filename, '')

            elif filename in cur_index:
                eid = (self.index.get(where('filename') == filename)).eid
                self.index.remove(eids=[eid])
                self.printFileEID('REMOVED', filename, eid)

    def genFileList(self, filelist):
        """Generate file list from directory tree, excluding binaries."""
        def walkdir(dirname):
            """Walk Directory and make filelist."""
            results = list()
            for dirpath, dirnames, files in os.walk(dirname):
                for name in files:
                    if name:
                        results.append('{}'.format(os.path.join(dirpath, name)))
            return results

        results = list()
        for filename in filelist:
            if os.path.isdir(filename):
                fl = walkdir(filename)
                for f in fl:
                    try:
                        if (magic.from_file(f, mime=True) != (b'application/octet-stream')):
                            results.append(f)
                    except FileNotFoundError:
                        pass
            elif filename != self.filename:
                try:
                    if (magic.from_file(filename, mime=True) != (b'application/octet-stream')):
                        results.append(filename)
                except FileNotFoundError:
                    pass

        return results

    def indexList(self, filelist):
        """List files found in the index."""
        cur_index = self.getCurrentIndex()
        for filename in filelist:
            if filename not in cur_index:
                self.printFileEID('NOT FOUND', filename, '')

            elif filename in cur_index:
                eid = (self.index.get(where('filename') == filename)).eid
                self.printFileEID('FOUND', filename, eid)

    def printFileEID(self, action, filename, eid):
        """Format and print results from query."""
        if action == 'NOT FOUND':
            print('{}NOT FOUND: {}{}'
                  .format(colors.fg.purple,
                          colors.fg.darkgrey, filename))
        else:
            print('{}{}: {}{} at {}EID: {}{}'
                  .format(colors.fg.purple, action,
                          colors.fg.darkgrey, filename,
                          colors.fg.purple,
                          colors.fg.darkgrey, eid))

    def returnhash(self, blob):
        """Return sha1sum of manifest."""
        return hashlib.sha1(blob.encode('UTF-8')).hexdigest()

    def lastFileSHA(self, filename):
        """Get SHA from last F card file found in."""
        try:
            num = self.getLastManifest().eid
            while num >= 1:
                fcards = self.getManifestByEID(num)['F']
                for sha, fn in fcards.items():
                    if fn == filename:
                        return(sha)
                num -= 1
        except AttributeError:
            return None

    def getDiff(self, filelist):
            """Return GNU Style diff. Current Implementation uses DeepDiff."""
            diffResults = str()
            results = []
            for filename in filelist:
                sha = self.lastFileSHA(filename)
                try:
                    blob = str(self.artifacts.search(where('ID') == sha)[0]['Blob'])
                except IndexError:
                    blob = ''
                try:
                    with open(filename) as f:
                        contents = f.read()
                except FileNotFoundError:
                    break
                if sha != self.returnhash(contents):
                    diffObj = DeepDiff(blob, contents)
                    diffResults += ('{}\n'.format(
                                    diffObj['values_changed']
                                           ["root"]["diff"]))
                    results.append(filename)

            return(diffResults, results)

    def readfile(self, file):
        """Read contents of file."""
        with open(file, 'r') as f:
            data = f.read()
        return data

    def totext(self, manifest):
        """Parse manifest to text."""
        str = ""
        for k, v in manifest.items():
            if k == 'F':
                for sv in v:
                    str += '{} {} {}\n'.format(k, sv, v[sv])
            else:
                str += '{} {}\n'.format(k, v)
        return(str)

    def hashfile(self, filepath):
        """Hash file at filepath."""
        sha = hashlib.sha1()
        with open(filepath, 'rb') as f:
            while True:
                block = f.read(2 ** 10)  # Magic number: one-megabyte blocks.
                if not block:
                    break
                sha.update(block)
            return sha.hexdigest()
