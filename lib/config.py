#!/usr/bin/env python3
"""config.py trivial-python-vcs configuration options.

The MIT License (MIT)
Copyright (c) 2016 Dustin Keib

All other works copyright their respective owners
"""

DEBUG = 'True'
WRITEARTIFACTS = 'False'
DEFAULTBRANCH = 'master'  # 30
CACHE_ARTIFACTS = None  # 30
CACHE_MANIFESTS = None  # 30
CACHE_BRANCHES = None  # 30
CACHE_INDEX = None  # 30
CACHE_CONFIGS = None  # 30
